<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_aecb4743d59522e0630beb56382f9c04afb12c021a6d0e8cae05866e91a35c7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7bfcfc6c278627a2791f0bb4949cf49b90006997c96d98d33818408e20b3f4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7bfcfc6c278627a2791f0bb4949cf49b90006997c96d98d33818408e20b3f4e->enter($__internal_d7bfcfc6c278627a2791f0bb4949cf49b90006997c96d98d33818408e20b3f4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_e60eb2afabbe0cf1ae754510660c4134a4c9942fb572990b35652130016afe2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e60eb2afabbe0cf1ae754510660c4134a4c9942fb572990b35652130016afe2e->enter($__internal_e60eb2afabbe0cf1ae754510660c4134a4c9942fb572990b35652130016afe2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 3
        echo "
";
        
        $__internal_d7bfcfc6c278627a2791f0bb4949cf49b90006997c96d98d33818408e20b3f4e->leave($__internal_d7bfcfc6c278627a2791f0bb4949cf49b90006997c96d98d33818408e20b3f4e_prof);

        
        $__internal_e60eb2afabbe0cf1ae754510660c4134a4c9942fb572990b35652130016afe2e->leave($__internal_e60eb2afabbe0cf1ae754510660c4134a4c9942fb572990b35652130016afe2e_prof);

    }

    // line 1
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_af79062a14607c0c56380206eebab31b3cdc7ca8c7b09c3d1608fceab966ed8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af79062a14607c0c56380206eebab31b3cdc7ca8c7b09c3d1608fceab966ed8a->enter($__internal_af79062a14607c0c56380206eebab31b3cdc7ca8c7b09c3d1608fceab966ed8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7315ec3644de66d86cf2b33d2b1a38b7607b9a135052cb3a932b662cabd4670e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7315ec3644de66d86cf2b33d2b1a38b7607b9a135052cb3a932b662cabd4670e->enter($__internal_7315ec3644de66d86cf2b33d2b1a38b7607b9a135052cb3a932b662cabd4670e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_7315ec3644de66d86cf2b33d2b1a38b7607b9a135052cb3a932b662cabd4670e->leave($__internal_7315ec3644de66d86cf2b33d2b1a38b7607b9a135052cb3a932b662cabd4670e_prof);

        
        $__internal_af79062a14607c0c56380206eebab31b3cdc7ca8c7b09c3d1608fceab966ed8a->leave($__internal_af79062a14607c0c56380206eebab31b3cdc7ca8c7b09c3d1608fceab966ed8a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  39 => 1,  28 => 3,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block fos_user_content %}
{% endblock fos_user_content %}

", "@FOSUser/layout.html.twig", "/var/www/html/codesnippets/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
