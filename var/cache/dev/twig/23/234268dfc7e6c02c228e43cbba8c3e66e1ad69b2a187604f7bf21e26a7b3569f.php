<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_3adee3f3af9b2873b2fcb3e9031ea46d2732e1b0b1b70c4f23aaf3bad14154a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/Security/login_content.html.twig", 3);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'preBody' => array($this, 'block_preBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0c54c0c91178fc3d770d49db68d47c0c994d963bd79622f2d096635c298469d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0c54c0c91178fc3d770d49db68d47c0c994d963bd79622f2d096635c298469d->enter($__internal_d0c54c0c91178fc3d770d49db68d47c0c994d963bd79622f2d096635c298469d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_aac9604f92ae0054260ba7a3b4affd067d8208eb2d1ddbb47e03d171ca3d2571 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aac9604f92ae0054260ba7a3b4affd067d8208eb2d1ddbb47e03d171ca3d2571->enter($__internal_aac9604f92ae0054260ba7a3b4affd067d8208eb2d1ddbb47e03d171ca3d2571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0c54c0c91178fc3d770d49db68d47c0c994d963bd79622f2d096635c298469d->leave($__internal_d0c54c0c91178fc3d770d49db68d47c0c994d963bd79622f2d096635c298469d_prof);

        
        $__internal_aac9604f92ae0054260ba7a3b4affd067d8208eb2d1ddbb47e03d171ca3d2571->leave($__internal_aac9604f92ae0054260ba7a3b4affd067d8208eb2d1ddbb47e03d171ca3d2571_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a49c8dac7cdd2f33c35b941ddb0bc37bc7de7d94296a715fe801c26501d99001 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a49c8dac7cdd2f33c35b941ddb0bc37bc7de7d94296a715fe801c26501d99001->enter($__internal_a49c8dac7cdd2f33c35b941ddb0bc37bc7de7d94296a715fe801c26501d99001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_80defcbb29fa5f7d428da0870595e4799158d68a7c68cb99f63d1e47d8f8df29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80defcbb29fa5f7d428da0870595e4799158d68a7c68cb99f63d1e47d8f8df29->enter($__internal_80defcbb29fa5f7d428da0870595e4799158d68a7c68cb99f63d1e47d8f8df29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_80defcbb29fa5f7d428da0870595e4799158d68a7c68cb99f63d1e47d8f8df29->leave($__internal_80defcbb29fa5f7d428da0870595e4799158d68a7c68cb99f63d1e47d8f8df29_prof);

        
        $__internal_a49c8dac7cdd2f33c35b941ddb0bc37bc7de7d94296a715fe801c26501d99001->leave($__internal_a49c8dac7cdd2f33c35b941ddb0bc37bc7de7d94296a715fe801c26501d99001_prof);

    }

    // line 7
    public function block_preBody($context, array $blocks = array())
    {
        $__internal_ef8a7af94f674388983fe1162f839e5d610c0e4ed5280e4bdd2157d43264f63e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef8a7af94f674388983fe1162f839e5d610c0e4ed5280e4bdd2157d43264f63e->enter($__internal_ef8a7af94f674388983fe1162f839e5d610c0e4ed5280e4bdd2157d43264f63e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preBody"));

        $__internal_24ee222ac247210e7574462491e08d1224cba630d6f265a5ac81ade2b4d1a806 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24ee222ac247210e7574462491e08d1224cba630d6f265a5ac81ade2b4d1a806->enter($__internal_24ee222ac247210e7574462491e08d1224cba630d6f265a5ac81ade2b4d1a806_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preBody"));

        // line 8
        echo "    <body class=\"login-layout\">
    <div class=\"main-container\">
        <div class=\"main-content\">
            ";
        // line 11
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 11, $this->getSourceContext()); })())) {
            // line 12
            echo "                <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 14
        echo "
            <div class=\"row\">
                <div class=\"col-sm-10 col-sm-offset-1\">
                    <div class=\"login-container\">
                        <div class=\"center\">
                            <h3>
                                <i class=\"ace-icon fa fa-code white\"></i>
                                <span class=\"white\" id=\"id-text2\"><b style=\"font-weight: 900\">Code</b>Snippets</span>
                            </h3>
                        </div>

                        <div class=\"space-6\"></div>

                        <div class=\"position-relative\">
                            <div id=\"login-box\" class=\"login-box visible widget-box no-border\">
                                <div class=\"widget-body\">
                                    <div class=\"widget-main\">
                                        <h4 class=\"header blue lighter bigger\">
                                            <i class=\"ace-icon fa fa-user green\"></i>
                                            Anmelden
                                        </h4>

                                        <div class=\"space-6\"></div>


                                        <form action=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                                            <fieldset>
                                                ";
        // line 41
        if ((isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 41, $this->getSourceContext()); })())) {
            // line 42
            echo "                                                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 42, $this->getSourceContext()); })()), "html", null, true);
            echo "\" />
                                                ";
        }
        // line 44
        echo "
                                                <label class=\"block clearfix\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"block input-icon input-icon-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("login.username", array(), "forms"), "html", null, true);
        echo "\" id=\"username\" name=\"_username\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 47, $this->getSourceContext()); })()), "html", null, true);
        echo "\" required=\"required\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
                                                </label>

                                                <label class=\"block clearfix\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"block input-icon input-icon-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("login.password", array(), "forms"), "html", null, true);
        echo "\" name=\"_password\" required=\"required\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-lock\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
                                                </label>

                                                <div class=\"space\"></div>

                                                <div class=\"clearfix\">
                                                    <label class=\"inline\">
                                                        <label for=\"remember_me\">";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("login.remember_me", array(), "forms"), "html", null, true);
        echo "</label>
                                                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                                                    </label>

                                                    <input type=\"submit\" id=\"_submit\" class=\"width-35 pull-right btn btn-sm btn-primary\" name=\"_submit\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("login.submit", array(), "forms"), "html", null, true);
        echo "\" />

                                                </div>

                                                <div class=\"space-4\"></div>

                                                <a href=\"";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("register");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("login.register", array(), "forms"), "html", null, true);
        echo "</a>
                                            </fieldset>
                                        </form>

                                    </div><!-- /.widget-main -->

                                </div><!-- /.widget-body -->
                            </div><!-- /.login-box -->


                        </div><!-- /.position-relative -->

                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.main-content -->
    </div>
    </body>
";
        
        $__internal_24ee222ac247210e7574462491e08d1224cba630d6f265a5ac81ade2b4d1a806->leave($__internal_24ee222ac247210e7574462491e08d1224cba630d6f265a5ac81ade2b4d1a806_prof);

        
        $__internal_ef8a7af94f674388983fe1162f839e5d610c0e4ed5280e4bdd2157d43264f63e->leave($__internal_ef8a7af94f674388983fe1162f839e5d610c0e4ed5280e4bdd2157d43264f63e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 73,  156 => 67,  149 => 63,  137 => 54,  125 => 47,  120 => 44,  114 => 42,  112 => 41,  107 => 39,  80 => 14,  74 => 12,  72 => 11,  67 => 8,  58 => 7,  41 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'AdminBundle' %}

{% extends 'base.html.twig' %}

{% block title %}{% endblock %}

{% block preBody %}
    <body class=\"login-layout\">
    <div class=\"main-container\">
        <div class=\"main-content\">
            {% if error %}
                <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
            {% endif %}

            <div class=\"row\">
                <div class=\"col-sm-10 col-sm-offset-1\">
                    <div class=\"login-container\">
                        <div class=\"center\">
                            <h3>
                                <i class=\"ace-icon fa fa-code white\"></i>
                                <span class=\"white\" id=\"id-text2\"><b style=\"font-weight: 900\">Code</b>Snippets</span>
                            </h3>
                        </div>

                        <div class=\"space-6\"></div>

                        <div class=\"position-relative\">
                            <div id=\"login-box\" class=\"login-box visible widget-box no-border\">
                                <div class=\"widget-body\">
                                    <div class=\"widget-main\">
                                        <h4 class=\"header blue lighter bigger\">
                                            <i class=\"ace-icon fa fa-user green\"></i>
                                            Anmelden
                                        </h4>

                                        <div class=\"space-6\"></div>


                                        <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
                                            <fieldset>
                                                {% if csrf_token %}
                                                    <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
                                                {% endif %}

                                                <label class=\"block clearfix\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"block input-icon input-icon-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"{{ 'login.username'|trans({},'forms') }}\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
                                                </label>

                                                <label class=\"block clearfix\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"block input-icon input-icon-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"{{ 'login.password'|trans({},'forms') }}\" name=\"_password\" required=\"required\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-lock\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
                                                </label>

                                                <div class=\"space\"></div>

                                                <div class=\"clearfix\">
                                                    <label class=\"inline\">
                                                        <label for=\"remember_me\">{{ 'login.remember_me'|trans({},'forms') }}</label>
                                                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                                                    </label>

                                                    <input type=\"submit\" id=\"_submit\" class=\"width-35 pull-right btn btn-sm btn-primary\" name=\"_submit\" value=\"{{ 'login.submit'|trans({},'forms') }}\" />

                                                </div>

                                                <div class=\"space-4\"></div>

                                                <a href=\"{{ url('register') }}\">{{ 'login.register'|trans({},'forms') }}</a>
                                            </fieldset>
                                        </form>

                                    </div><!-- /.widget-main -->

                                </div><!-- /.widget-body -->
                            </div><!-- /.login-box -->


                        </div><!-- /.position-relative -->

                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.main-content -->
    </div>
    </body>
{% endblock %}











", "@FOSUser/Security/login_content.html.twig", "/var/www/html/codesnippets/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
