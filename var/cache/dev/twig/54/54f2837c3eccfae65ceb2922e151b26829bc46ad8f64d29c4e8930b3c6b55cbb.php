<?php

/* navbar.html.twig */
class __TwigTemplate_9d371db52a551ccaae9db16ba769051cd1138a72e9a24c909be1139f684044b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b760f8e9f349b54f33990cf78094b6f28a48463bef52f3a30c7a2295dd6e057f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b760f8e9f349b54f33990cf78094b6f28a48463bef52f3a30c7a2295dd6e057f->enter($__internal_b760f8e9f349b54f33990cf78094b6f28a48463bef52f3a30c7a2295dd6e057f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        $__internal_c30873b42cbd155479f8ebd74ac4eb4464424e283aa3c3ffda418fc64506e30c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c30873b42cbd155479f8ebd74ac4eb4464424e283aa3c3ffda418fc64506e30c->enter($__internal_c30873b42cbd155479f8ebd74ac4eb4464424e283aa3c3ffda418fc64506e30c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        // line 2
        echo "
<div id=\"navbar\" class=\"navbar navbar-default\">
    <script type=\"text/javascript\">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class=\"navbar-container\" id=\"navbar-container\">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type=\"button\" class=\"navbar-toggle menu-toggler pull-left\" id=\"menu-toggler\">
            <span class=\"sr-only\">Toggle sidebar</span>

            <span class=\"icon-bar\"></span>

            <span class=\"icon-bar\"></span>

            <span class=\"icon-bar\"></span>
        </button>

        <!-- /section:basics/sidebar.mobile.toggle -->
        <div class=\"navbar-header pull-left\">
            <!-- #section:basics/navbar.layout.brand -->
            <a href=\"#\" class=\"navbar-brand\">
                <small>
                    <i class=\"fa fa-code\"></i>
                    <b style=\"font-weight: 900\">Code</b>Snippets
                </small>
            </a>

            <!-- /section:basics/navbar.layout.brand -->

            <!-- #section:basics/navbar.toggle -->

            <!-- /section:basics/navbar.toggle -->
        </div>

        <!-- #section:basics/navbar.dropdown -->
        <div class=\"navbar-buttons navbar-header pull-right\" role=\"navigation\">
            <ul class=\"nav ace-nav\">
                <li class=\"purple\">
                    <a data-toggle=\"dropdown\" href=\"#\" class=\"dropdown-toggle\">
                        <i class=\"ace-icon fa fa-cloud-download\"></i>
                    </a>

                    <ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">
                        <li>
                            <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/app/extensions/chrome/CodeSnippets.crx"), "html", null, true);
        echo "\">
                                <i class=\"ace-icon fa fa-chrome\"></i>
                                Chrome Extension
                            </a>
                        </li>
                        ";
        // line 58
        echo "
                    </ul>
                </li>


                <!-- #section:basics/navbar.user_menu -->
                <li class=\"light-blue\">
                    <a data-toggle=\"dropdown\" href=\"#\" class=\"dropdown-toggle\">
                        <img class=\"nav-user-photo\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/avatars/avatar5.png"), "html", null, true);
        echo "\" alt=\"User's Photo\" />
                        <span class=\"user-info\">
\t\t\t\t\t\t\t\t\t<small>";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.welcome", array(), "navbar"), "html", null, true);
        echo ",</small>
\t\t\t\t\t\t\t\t\t";
        // line 69
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 69, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t</span>

                        <i class=\"ace-icon fa fa-caret-down\"></i>
                    </a>

                    <ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">
                        <li>
                            <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("profile_edit");
        echo "\">
                                <i class=\"ace-icon fa fa-cog\"></i>
                                ";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings", array(), "navbar"), "html", null, true);
        echo "
                            </a>
                        </li>

                        <li>
                            <a href=\"";
        // line 84
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("profile_show");
        echo "\">
                                <i class=\"ace-icon fa fa-user\"></i>
                                ";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.profile", array(), "navbar"), "html", null, true);
        echo "
                            </a>
                        </li>

                        <li class=\"divider\"></li>

                        <li>
                            <a href=\"";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\">
                                <i class=\"ace-icon fa fa-power-off\"></i>
                                ";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.logout", array(), "navbar"), "html", null, true);
        echo "
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- /section:basics/navbar.user_menu -->
            </ul>
        </div>

        <!-- /section:basics/navbar.dropdown -->
    </div><!-- /.navbar-container -->
</div>";
        
        $__internal_b760f8e9f349b54f33990cf78094b6f28a48463bef52f3a30c7a2295dd6e057f->leave($__internal_b760f8e9f349b54f33990cf78094b6f28a48463bef52f3a30c7a2295dd6e057f_prof);

        
        $__internal_c30873b42cbd155479f8ebd74ac4eb4464424e283aa3c3ffda418fc64506e30c->leave($__internal_c30873b42cbd155479f8ebd74ac4eb4464424e283aa3c3ffda418fc64506e30c_prof);

    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 95,  138 => 93,  128 => 86,  123 => 84,  115 => 79,  110 => 77,  99 => 69,  95 => 68,  90 => 66,  80 => 58,  72 => 47,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'AdminBundle' %}

<div id=\"navbar\" class=\"navbar navbar-default\">
    <script type=\"text/javascript\">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class=\"navbar-container\" id=\"navbar-container\">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type=\"button\" class=\"navbar-toggle menu-toggler pull-left\" id=\"menu-toggler\">
            <span class=\"sr-only\">Toggle sidebar</span>

            <span class=\"icon-bar\"></span>

            <span class=\"icon-bar\"></span>

            <span class=\"icon-bar\"></span>
        </button>

        <!-- /section:basics/sidebar.mobile.toggle -->
        <div class=\"navbar-header pull-left\">
            <!-- #section:basics/navbar.layout.brand -->
            <a href=\"#\" class=\"navbar-brand\">
                <small>
                    <i class=\"fa fa-code\"></i>
                    <b style=\"font-weight: 900\">Code</b>Snippets
                </small>
            </a>

            <!-- /section:basics/navbar.layout.brand -->

            <!-- #section:basics/navbar.toggle -->

            <!-- /section:basics/navbar.toggle -->
        </div>

        <!-- #section:basics/navbar.dropdown -->
        <div class=\"navbar-buttons navbar-header pull-right\" role=\"navigation\">
            <ul class=\"nav ace-nav\">
                <li class=\"purple\">
                    <a data-toggle=\"dropdown\" href=\"#\" class=\"dropdown-toggle\">
                        <i class=\"ace-icon fa fa-cloud-download\"></i>
                    </a>

                    <ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">
                        <li>
                            <a href=\"{{ asset('bundles/app/extensions/chrome/CodeSnippets.crx') }}\">
                                <i class=\"ace-icon fa fa-chrome\"></i>
                                Chrome Extension
                            </a>
                        </li>
                        {#<li>
                            <a href=\"#}{#{{ asset('bundles/app/extensions/chrome/CodeSnippets.crx') }}#}{##\">
                                <i class=\"ace-icon fa fa-firefox\"></i>
                                Firefox Extension
                            </a>
                        </li>#}

                    </ul>
                </li>


                <!-- #section:basics/navbar.user_menu -->
                <li class=\"light-blue\">
                    <a data-toggle=\"dropdown\" href=\"#\" class=\"dropdown-toggle\">
                        <img class=\"nav-user-photo\" src=\"{{ asset('bundles/admin/avatars/avatar5.png') }}\" alt=\"User's Photo\" />
                        <span class=\"user-info\">
\t\t\t\t\t\t\t\t\t<small>{{ 'user.welcome'|trans({},'navbar') }},</small>
\t\t\t\t\t\t\t\t\t{{ app.user.username }}
\t\t\t\t\t\t\t\t</span>

                        <i class=\"ace-icon fa fa-caret-down\"></i>
                    </a>

                    <ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">
                        <li>
                            <a href=\"{{ url('profile_edit') }}\">
                                <i class=\"ace-icon fa fa-cog\"></i>
                                {{ 'user.settings'|trans({},'navbar') }}
                            </a>
                        </li>

                        <li>
                            <a href=\"{{ url('profile_show') }}\">
                                <i class=\"ace-icon fa fa-user\"></i>
                                {{ 'user.profile'|trans({},'navbar') }}
                            </a>
                        </li>

                        <li class=\"divider\"></li>

                        <li>
                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                <i class=\"ace-icon fa fa-power-off\"></i>
                                {{ 'user.logout'|trans({},'navbar') }}
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- /section:basics/navbar.user_menu -->
            </ul>
        </div>

        <!-- /section:basics/navbar.dropdown -->
    </div><!-- /.navbar-container -->
</div>", "navbar.html.twig", "/var/www/html/codesnippets/app/Resources/views/navbar.html.twig");
    }
}
