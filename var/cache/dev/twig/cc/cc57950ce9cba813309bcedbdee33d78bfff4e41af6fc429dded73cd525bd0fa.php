<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_a8758d89990a8e032409d489571e6e7c6de6dbb562094e3e5e255d7dde23c3fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b65964e4d48dde1466878d8ccc902c0c4478ff717e336e2f25d073f3f2ad7df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b65964e4d48dde1466878d8ccc902c0c4478ff717e336e2f25d073f3f2ad7df->enter($__internal_9b65964e4d48dde1466878d8ccc902c0c4478ff717e336e2f25d073f3f2ad7df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_9604c754ca59ecda92edcecf030bd4036e0f6c006b278188a67f71dcf1fe09d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9604c754ca59ecda92edcecf030bd4036e0f6c006b278188a67f71dcf1fe09d9->enter($__internal_9604c754ca59ecda92edcecf030bd4036e0f6c006b278188a67f71dcf1fe09d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9b65964e4d48dde1466878d8ccc902c0c4478ff717e336e2f25d073f3f2ad7df->leave($__internal_9b65964e4d48dde1466878d8ccc902c0c4478ff717e336e2f25d073f3f2ad7df_prof);

        
        $__internal_9604c754ca59ecda92edcecf030bd4036e0f6c006b278188a67f71dcf1fe09d9->leave($__internal_9604c754ca59ecda92edcecf030bd4036e0f6c006b278188a67f71dcf1fe09d9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3a9fc454d0334a28b094c69141672e218ababc033371523ae2362aeaefa8d62b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a9fc454d0334a28b094c69141672e218ababc033371523ae2362aeaefa8d62b->enter($__internal_3a9fc454d0334a28b094c69141672e218ababc033371523ae2362aeaefa8d62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1c9cc829ad38f5f2851952f4b3477d7927786ec879cab006b698581abe541176 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c9cc829ad38f5f2851952f4b3477d7927786ec879cab006b698581abe541176->enter($__internal_1c9cc829ad38f5f2851952f4b3477d7927786ec879cab006b698581abe541176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_1c9cc829ad38f5f2851952f4b3477d7927786ec879cab006b698581abe541176->leave($__internal_1c9cc829ad38f5f2851952f4b3477d7927786ec879cab006b698581abe541176_prof);

        
        $__internal_3a9fc454d0334a28b094c69141672e218ababc033371523ae2362aeaefa8d62b->leave($__internal_3a9fc454d0334a28b094c69141672e218ababc033371523ae2362aeaefa8d62b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/var/www/html/codesnippets/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
