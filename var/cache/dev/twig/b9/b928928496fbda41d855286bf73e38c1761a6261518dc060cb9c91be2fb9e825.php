<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_37ca8c2051868c0fd10fed47d6466a108cc3948f09b9d2e96c70e73b0293b5ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3dde3c2ab194717e3be6115191e33bf97c5de0e62e42e1763e483ffae7c23555 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3dde3c2ab194717e3be6115191e33bf97c5de0e62e42e1763e483ffae7c23555->enter($__internal_3dde3c2ab194717e3be6115191e33bf97c5de0e62e42e1763e483ffae7c23555_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_43200835ecb29e68e7a9b726ee0bfa43fe81325866c112ade19dd7af3462d3d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43200835ecb29e68e7a9b726ee0bfa43fe81325866c112ade19dd7af3462d3d6->enter($__internal_43200835ecb29e68e7a9b726ee0bfa43fe81325866c112ade19dd7af3462d3d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3dde3c2ab194717e3be6115191e33bf97c5de0e62e42e1763e483ffae7c23555->leave($__internal_3dde3c2ab194717e3be6115191e33bf97c5de0e62e42e1763e483ffae7c23555_prof);

        
        $__internal_43200835ecb29e68e7a9b726ee0bfa43fe81325866c112ade19dd7af3462d3d6->leave($__internal_43200835ecb29e68e7a9b726ee0bfa43fe81325866c112ade19dd7af3462d3d6_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6c3ed900104babd5e75f20c81affaa7c87dba14bbaef6b76786f7dac22615e90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c3ed900104babd5e75f20c81affaa7c87dba14bbaef6b76786f7dac22615e90->enter($__internal_6c3ed900104babd5e75f20c81affaa7c87dba14bbaef6b76786f7dac22615e90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2febf63b2fc36d31f582b21cf58bb0d4bc2776ad2ae25c361f2b5a4c8c9102dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2febf63b2fc36d31f582b21cf58bb0d4bc2776ad2ae25c361f2b5a4c8c9102dc->enter($__internal_2febf63b2fc36d31f582b21cf58bb0d4bc2776ad2ae25c361f2b5a4c8c9102dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2febf63b2fc36d31f582b21cf58bb0d4bc2776ad2ae25c361f2b5a4c8c9102dc->leave($__internal_2febf63b2fc36d31f582b21cf58bb0d4bc2776ad2ae25c361f2b5a4c8c9102dc_prof);

        
        $__internal_6c3ed900104babd5e75f20c81affaa7c87dba14bbaef6b76786f7dac22615e90->leave($__internal_6c3ed900104babd5e75f20c81affaa7c87dba14bbaef6b76786f7dac22615e90_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_1a74e50f6bf2a38e45140aa8618871e9984c8eef5c4001265b27a6723647782c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a74e50f6bf2a38e45140aa8618871e9984c8eef5c4001265b27a6723647782c->enter($__internal_1a74e50f6bf2a38e45140aa8618871e9984c8eef5c4001265b27a6723647782c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_c4a61585c091e8bc4bf39434bd991359a708e094c53395954e63a623d495cdc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4a61585c091e8bc4bf39434bd991359a708e094c53395954e63a623d495cdc5->enter($__internal_c4a61585c091e8bc4bf39434bd991359a708e094c53395954e63a623d495cdc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_c4a61585c091e8bc4bf39434bd991359a708e094c53395954e63a623d495cdc5->leave($__internal_c4a61585c091e8bc4bf39434bd991359a708e094c53395954e63a623d495cdc5_prof);

        
        $__internal_1a74e50f6bf2a38e45140aa8618871e9984c8eef5c4001265b27a6723647782c->leave($__internal_1a74e50f6bf2a38e45140aa8618871e9984c8eef5c4001265b27a6723647782c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_eff785b69de788d24aaf4cc61f3e77007da44eaf9d85beb3776d56b2243ec7d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eff785b69de788d24aaf4cc61f3e77007da44eaf9d85beb3776d56b2243ec7d4->enter($__internal_eff785b69de788d24aaf4cc61f3e77007da44eaf9d85beb3776d56b2243ec7d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_20b312c7ff8fa80c9a7693ec0b7d6492b510916b5cbb80098a89b168e6852780 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20b312c7ff8fa80c9a7693ec0b7d6492b510916b5cbb80098a89b168e6852780->enter($__internal_20b312c7ff8fa80c9a7693ec0b7d6492b510916b5cbb80098a89b168e6852780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_20b312c7ff8fa80c9a7693ec0b7d6492b510916b5cbb80098a89b168e6852780->leave($__internal_20b312c7ff8fa80c9a7693ec0b7d6492b510916b5cbb80098a89b168e6852780_prof);

        
        $__internal_eff785b69de788d24aaf4cc61f3e77007da44eaf9d85beb3776d56b2243ec7d4->leave($__internal_eff785b69de788d24aaf4cc61f3e77007da44eaf9d85beb3776d56b2243ec7d4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/var/www/html/codesnippets/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
