<?php

/* AdminBundle:Default:index.html.twig */
class __TwigTemplate_391d645593daf06b92f97d3f756c02502e31e916d075c678ae73ab5dd01d03a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "AdminBundle:Default:index.html.twig", 3);
        $this->blocks = array(
            'parent' => array($this, 'block_parent'),
            'child' => array($this, 'block_child'),
            'contentHeadline' => array($this, 'block_contentHeadline'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42f91f9cfb32c0554860ae1a414d0590304b6b9f63d164e44f6fcd935a5f4755 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42f91f9cfb32c0554860ae1a414d0590304b6b9f63d164e44f6fcd935a5f4755->enter($__internal_42f91f9cfb32c0554860ae1a414d0590304b6b9f63d164e44f6fcd935a5f4755_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:index.html.twig"));

        $__internal_bed3115dbbecafe6de5b0d6f23afcc2f90cb6548fdc9235ff02b34d241d1e6e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bed3115dbbecafe6de5b0d6f23afcc2f90cb6548fdc9235ff02b34d241d1e6e7->enter($__internal_bed3115dbbecafe6de5b0d6f23afcc2f90cb6548fdc9235ff02b34d241d1e6e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42f91f9cfb32c0554860ae1a414d0590304b6b9f63d164e44f6fcd935a5f4755->leave($__internal_42f91f9cfb32c0554860ae1a414d0590304b6b9f63d164e44f6fcd935a5f4755_prof);

        
        $__internal_bed3115dbbecafe6de5b0d6f23afcc2f90cb6548fdc9235ff02b34d241d1e6e7->leave($__internal_bed3115dbbecafe6de5b0d6f23afcc2f90cb6548fdc9235ff02b34d241d1e6e7_prof);

    }

    // line 5
    public function block_parent($context, array $blocks = array())
    {
        $__internal_4a3a5fbfd0648ed6cf73b74b6f171a7b6d48578602b222b27743b551592b53ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a3a5fbfd0648ed6cf73b74b6f171a7b6d48578602b222b27743b551592b53ea->enter($__internal_4a3a5fbfd0648ed6cf73b74b6f171a7b6d48578602b222b27743b551592b53ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "parent"));

        $__internal_417818269fea446c0541bcb86ff3c14af836c3f16711fda4c46bbc90db301168 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_417818269fea446c0541bcb86ff3c14af836c3f16711fda4c46bbc90db301168->enter($__internal_417818269fea446c0541bcb86ff3c14af836c3f16711fda4c46bbc90db301168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "parent"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("dashboard.parent", array(), "sidebar"), "html", null, true);
        
        $__internal_417818269fea446c0541bcb86ff3c14af836c3f16711fda4c46bbc90db301168->leave($__internal_417818269fea446c0541bcb86ff3c14af836c3f16711fda4c46bbc90db301168_prof);

        
        $__internal_4a3a5fbfd0648ed6cf73b74b6f171a7b6d48578602b222b27743b551592b53ea->leave($__internal_4a3a5fbfd0648ed6cf73b74b6f171a7b6d48578602b222b27743b551592b53ea_prof);

    }

    // line 6
    public function block_child($context, array $blocks = array())
    {
        $__internal_0f4133fed1e1dcd688c1b086d7d7779cd3186ced9a8fedd0243998474cc699f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f4133fed1e1dcd688c1b086d7d7779cd3186ced9a8fedd0243998474cc699f6->enter($__internal_0f4133fed1e1dcd688c1b086d7d7779cd3186ced9a8fedd0243998474cc699f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "child"));

        $__internal_d9843cfa400307da39428cff1af50f4d95d496b1862e6af8b0f4f53f5fb97e0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9843cfa400307da39428cff1af50f4d95d496b1862e6af8b0f4f53f5fb97e0d->enter($__internal_d9843cfa400307da39428cff1af50f4d95d496b1862e6af8b0f4f53f5fb97e0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "child"));

        echo "...";
        
        $__internal_d9843cfa400307da39428cff1af50f4d95d496b1862e6af8b0f4f53f5fb97e0d->leave($__internal_d9843cfa400307da39428cff1af50f4d95d496b1862e6af8b0f4f53f5fb97e0d_prof);

        
        $__internal_0f4133fed1e1dcd688c1b086d7d7779cd3186ced9a8fedd0243998474cc699f6->leave($__internal_0f4133fed1e1dcd688c1b086d7d7779cd3186ced9a8fedd0243998474cc699f6_prof);

    }

    // line 7
    public function block_contentHeadline($context, array $blocks = array())
    {
        $__internal_f2e3deb5060bf8be4dc3707d2ed54f2817ca9654c02f5a695e78a3f9d9986be0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2e3deb5060bf8be4dc3707d2ed54f2817ca9654c02f5a695e78a3f9d9986be0->enter($__internal_f2e3deb5060bf8be4dc3707d2ed54f2817ca9654c02f5a695e78a3f9d9986be0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contentHeadline"));

        $__internal_ab13e1f9d180ed7b06861a135d34fbdc797fb63b191906ce4b2dccccd03a813d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab13e1f9d180ed7b06861a135d34fbdc797fb63b191906ce4b2dccccd03a813d->enter($__internal_ab13e1f9d180ed7b06861a135d34fbdc797fb63b191906ce4b2dccccd03a813d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contentHeadline"));

        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("dashboard.parent", array(), "sidebar"), "html", null, true);
        
        $__internal_ab13e1f9d180ed7b06861a135d34fbdc797fb63b191906ce4b2dccccd03a813d->leave($__internal_ab13e1f9d180ed7b06861a135d34fbdc797fb63b191906ce4b2dccccd03a813d_prof);

        
        $__internal_f2e3deb5060bf8be4dc3707d2ed54f2817ca9654c02f5a695e78a3f9d9986be0->leave($__internal_f2e3deb5060bf8be4dc3707d2ed54f2817ca9654c02f5a695e78a3f9d9986be0_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_2ffc6d4ff32992ed69e5f1a597b151ceebb0b7ad87a999ef9351a61fc457e72d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ffc6d4ff32992ed69e5f1a597b151ceebb0b7ad87a999ef9351a61fc457e72d->enter($__internal_2ffc6d4ff32992ed69e5f1a597b151ceebb0b7ad87a999ef9351a61fc457e72d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_c458a942ad40b56b03a6009e1111d9b9b6ccebd85ddca851ad4d48127b570a18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c458a942ad40b56b03a6009e1111d9b9b6ccebd85ddca851ad4d48127b570a18->enter($__internal_c458a942ad40b56b03a6009e1111d9b9b6ccebd85ddca851ad4d48127b570a18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
";
        
        $__internal_c458a942ad40b56b03a6009e1111d9b9b6ccebd85ddca851ad4d48127b570a18->leave($__internal_c458a942ad40b56b03a6009e1111d9b9b6ccebd85ddca851ad4d48127b570a18_prof);

        
        $__internal_2ffc6d4ff32992ed69e5f1a597b151ceebb0b7ad87a999ef9351a61fc457e72d->leave($__internal_2ffc6d4ff32992ed69e5f1a597b151ceebb0b7ad87a999ef9351a61fc457e72d_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 11,  98 => 10,  79 => 7,  61 => 6,  43 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'AdminBundle' %}

{% extends 'base.html.twig' %}

{% block parent %}{{ 'dashboard.parent'|trans({},'sidebar') }}{% endblock %}
{% block child %}...{% endblock %}
{% block contentHeadline %} {{ 'dashboard.parent'|trans({},'sidebar') }}{% endblock %}
{#{% block contentSubHeadline %}{% endblock %}#}

{% block content %}

{% endblock %}", "AdminBundle:Default:index.html.twig", "/var/www/html/codesnippets/src/AdminBundle/Resources/views/Default/index.html.twig");
    }
}
