<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_dbd00ee28b6d64ea416cd12c81a887e97ded7a96ff69fa517120e2878ade0426 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6be7d188f543b43c3403cce9993f3567a6e2262d64fa9f5f090c1ab3c75b488d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6be7d188f543b43c3403cce9993f3567a6e2262d64fa9f5f090c1ab3c75b488d->enter($__internal_6be7d188f543b43c3403cce9993f3567a6e2262d64fa9f5f090c1ab3c75b488d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_efbe590ce941fa58577c26d593d89b4b50e7f075d1952d15c72a18df5c65d738 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efbe590ce941fa58577c26d593d89b4b50e7f075d1952d15c72a18df5c65d738->enter($__internal_efbe590ce941fa58577c26d593d89b4b50e7f075d1952d15c72a18df5c65d738_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6be7d188f543b43c3403cce9993f3567a6e2262d64fa9f5f090c1ab3c75b488d->leave($__internal_6be7d188f543b43c3403cce9993f3567a6e2262d64fa9f5f090c1ab3c75b488d_prof);

        
        $__internal_efbe590ce941fa58577c26d593d89b4b50e7f075d1952d15c72a18df5c65d738->leave($__internal_efbe590ce941fa58577c26d593d89b4b50e7f075d1952d15c72a18df5c65d738_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1a038bb04ccbd3bb3da8ee30df5a4e4f15458b5246d5cc5c8fef18199f572bdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a038bb04ccbd3bb3da8ee30df5a4e4f15458b5246d5cc5c8fef18199f572bdb->enter($__internal_1a038bb04ccbd3bb3da8ee30df5a4e4f15458b5246d5cc5c8fef18199f572bdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_2fe9863dbe8a03d963be44ad4b7467e029cafccf5f03b6d220f246be755cc3cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fe9863dbe8a03d963be44ad4b7467e029cafccf5f03b6d220f246be755cc3cd->enter($__internal_2fe9863dbe8a03d963be44ad4b7467e029cafccf5f03b6d220f246be755cc3cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_2fe9863dbe8a03d963be44ad4b7467e029cafccf5f03b6d220f246be755cc3cd->leave($__internal_2fe9863dbe8a03d963be44ad4b7467e029cafccf5f03b6d220f246be755cc3cd_prof);

        
        $__internal_1a038bb04ccbd3bb3da8ee30df5a4e4f15458b5246d5cc5c8fef18199f572bdb->leave($__internal_1a038bb04ccbd3bb3da8ee30df5a4e4f15458b5246d5cc5c8fef18199f572bdb_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_8943714b2da26fd4e6954c07be809f7c77f369d3e6ff87c304b02efcb54e1a1e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8943714b2da26fd4e6954c07be809f7c77f369d3e6ff87c304b02efcb54e1a1e->enter($__internal_8943714b2da26fd4e6954c07be809f7c77f369d3e6ff87c304b02efcb54e1a1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f0f34f76a6e94a49911c5480e11d78da027a1539f50cf63b4d8b97a703a5befc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0f34f76a6e94a49911c5480e11d78da027a1539f50cf63b4d8b97a703a5befc->enter($__internal_f0f34f76a6e94a49911c5480e11d78da027a1539f50cf63b4d8b97a703a5befc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f0f34f76a6e94a49911c5480e11d78da027a1539f50cf63b4d8b97a703a5befc->leave($__internal_f0f34f76a6e94a49911c5480e11d78da027a1539f50cf63b4d8b97a703a5befc_prof);

        
        $__internal_8943714b2da26fd4e6954c07be809f7c77f369d3e6ff87c304b02efcb54e1a1e->leave($__internal_8943714b2da26fd4e6954c07be809f7c77f369d3e6ff87c304b02efcb54e1a1e_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_39078193705f35cdf510dd2dede3d4f6d3764347cc0766fe1ad0b6af2d1b179b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39078193705f35cdf510dd2dede3d4f6d3764347cc0766fe1ad0b6af2d1b179b->enter($__internal_39078193705f35cdf510dd2dede3d4f6d3764347cc0766fe1ad0b6af2d1b179b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b5cc8d11334b609c167f909298c7a17087e60567fc435fa395c39b60acc61f84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5cc8d11334b609c167f909298c7a17087e60567fc435fa395c39b60acc61f84->enter($__internal_b5cc8d11334b609c167f909298c7a17087e60567fc435fa395c39b60acc61f84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_b5cc8d11334b609c167f909298c7a17087e60567fc435fa395c39b60acc61f84->leave($__internal_b5cc8d11334b609c167f909298c7a17087e60567fc435fa395c39b60acc61f84_prof);

        
        $__internal_39078193705f35cdf510dd2dede3d4f6d3764347cc0766fe1ad0b6af2d1b179b->leave($__internal_39078193705f35cdf510dd2dede3d4f6d3764347cc0766fe1ad0b6af2d1b179b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/html/codesnippets/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
