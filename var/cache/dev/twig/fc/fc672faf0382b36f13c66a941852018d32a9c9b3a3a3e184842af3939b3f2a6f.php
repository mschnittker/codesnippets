<?php

/* base.html.twig */
class __TwigTemplate_01f03d2883c4a30bca746f0a6f2b1225931fe9c7b9ecd11176b7d7e595f49100 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'preBody' => array($this, 'block_preBody'),
            'body' => array($this, 'block_body'),
            'contentHeadline' => array($this, 'block_contentHeadline'),
            'content' => array($this, 'block_content'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3798f7e9a1ca59b5ced7b78f5db09812f0e41ad2589ebf61c58266331878e84f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3798f7e9a1ca59b5ced7b78f5db09812f0e41ad2589ebf61c58266331878e84f->enter($__internal_3798f7e9a1ca59b5ced7b78f5db09812f0e41ad2589ebf61c58266331878e84f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_b6a580b137a2bdb2364365cacd630361627de0c2d8e732e7077d35ddf36b31b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6a580b137a2bdb2364365cacd630361627de0c2d8e732e7077d35ddf36b31b9->enter($__internal_b6a580b137a2bdb2364365cacd630361627de0c2d8e732e7077d35ddf36b31b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
    <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/font-awesome.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/ace-fonts.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/ace.min.css"), "html", null, true);
        echo "\" />

    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/ace-skins.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/ace-rtl.min.css"), "html", null, true);
        echo "\" />

    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/ace-extra.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("built/css/main.css"), "html", null, true);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/prism.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("built/css/main.css"), "html", null, true);
        echo "\">

    ";
        // line 28
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 29
        echo "
</head>

";
        // line 32
        $this->displayBlock('preBody', $context, $blocks);
        // line 145
        echo "</html>";
        
        $__internal_3798f7e9a1ca59b5ced7b78f5db09812f0e41ad2589ebf61c58266331878e84f->leave($__internal_3798f7e9a1ca59b5ced7b78f5db09812f0e41ad2589ebf61c58266331878e84f_prof);

        
        $__internal_b6a580b137a2bdb2364365cacd630361627de0c2d8e732e7077d35ddf36b31b9->leave($__internal_b6a580b137a2bdb2364365cacd630361627de0c2d8e732e7077d35ddf36b31b9_prof);

    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        $__internal_a9dc08386e805447f1f5ede42f4ac08ee5bc56798c1f45663641be6f481e0f43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9dc08386e805447f1f5ede42f4ac08ee5bc56798c1f45663641be6f481e0f43->enter($__internal_a9dc08386e805447f1f5ede42f4ac08ee5bc56798c1f45663641be6f481e0f43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ebd2b90496377c8cb27310e700582610a3f7d754cf5e1c688ad6c0ff384e71de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebd2b90496377c8cb27310e700582610a3f7d754cf5e1c688ad6c0ff384e71de->enter($__internal_ebd2b90496377c8cb27310e700582610a3f7d754cf5e1c688ad6c0ff384e71de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_ebd2b90496377c8cb27310e700582610a3f7d754cf5e1c688ad6c0ff384e71de->leave($__internal_ebd2b90496377c8cb27310e700582610a3f7d754cf5e1c688ad6c0ff384e71de_prof);

        
        $__internal_a9dc08386e805447f1f5ede42f4ac08ee5bc56798c1f45663641be6f481e0f43->leave($__internal_a9dc08386e805447f1f5ede42f4ac08ee5bc56798c1f45663641be6f481e0f43_prof);

    }

    // line 28
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_2882c9f5a8a5280bd40f0f32d50fb9a99071f6cfba12ef921cf43dad0a1cdb88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2882c9f5a8a5280bd40f0f32d50fb9a99071f6cfba12ef921cf43dad0a1cdb88->enter($__internal_2882c9f5a8a5280bd40f0f32d50fb9a99071f6cfba12ef921cf43dad0a1cdb88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_4b56aeac8123c04ce304192c733724df8df20ab456d2d873340f39aaab4829b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b56aeac8123c04ce304192c733724df8df20ab456d2d873340f39aaab4829b0->enter($__internal_4b56aeac8123c04ce304192c733724df8df20ab456d2d873340f39aaab4829b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_4b56aeac8123c04ce304192c733724df8df20ab456d2d873340f39aaab4829b0->leave($__internal_4b56aeac8123c04ce304192c733724df8df20ab456d2d873340f39aaab4829b0_prof);

        
        $__internal_2882c9f5a8a5280bd40f0f32d50fb9a99071f6cfba12ef921cf43dad0a1cdb88->leave($__internal_2882c9f5a8a5280bd40f0f32d50fb9a99071f6cfba12ef921cf43dad0a1cdb88_prof);

    }

    // line 32
    public function block_preBody($context, array $blocks = array())
    {
        $__internal_bbe2e10895e7a00c30f5b129dbc7de6567db80c2d6b9b9e455526f5c2f57ad0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbe2e10895e7a00c30f5b129dbc7de6567db80c2d6b9b9e455526f5c2f57ad0d->enter($__internal_bbe2e10895e7a00c30f5b129dbc7de6567db80c2d6b9b9e455526f5c2f57ad0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preBody"));

        $__internal_cc24ba5ec20635bd7dc16380284862bac820a760a84483cf080884e6619bb5e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc24ba5ec20635bd7dc16380284862bac820a760a84483cf080884e6619bb5e2->enter($__internal_cc24ba5ec20635bd7dc16380284862bac820a760a84483cf080884e6619bb5e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preBody"));

        // line 33
        echo "<body class=\"no-skin\">


    ";
        // line 36
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 36, $this->getSourceContext()); })()), "request", array()), "hasPreviousSession", array())) {
            // line 37
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 37, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 38
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 39
                    echo "                <div class=\"alert alert-success alert-dismissable flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    ";
                    // line 41
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 44
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "    ";
        }
        // line 46
        echo "

    ";
        // line 48
        $this->displayBlock('body', $context, $blocks);
        // line 114
        echo "
    <script src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\">
        if('ontouchstart' in document.documentElement) document.write(\"<script src='./bundles/admin/js/jquery.mobile.custom.min.js'>\"+\"<\"+\"/script>\");
    </script>

    <script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/ace-elements.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/ace.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"stylesheet\" href=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/css/ace.onpage-help.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/themes/sunburst.css"), "html", null, true);
        echo "\" />

    <script type=\"text/javascript\"> ace.vars['base'] = '..'; </script>
    <script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/ace/ace.onpage-help.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/rainbow.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/language/generic.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/language/html.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/language/css.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/language/javascript.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/ace-extra.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/clipboard.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/prism.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("built/js/app.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 142
        $this->displayBlock('javascript', $context, $blocks);
        // line 143
        echo "</body>
";
        
        $__internal_cc24ba5ec20635bd7dc16380284862bac820a760a84483cf080884e6619bb5e2->leave($__internal_cc24ba5ec20635bd7dc16380284862bac820a760a84483cf080884e6619bb5e2_prof);

        
        $__internal_bbe2e10895e7a00c30f5b129dbc7de6567db80c2d6b9b9e455526f5c2f57ad0d->leave($__internal_bbe2e10895e7a00c30f5b129dbc7de6567db80c2d6b9b9e455526f5c2f57ad0d_prof);

    }

    // line 48
    public function block_body($context, array $blocks = array())
    {
        $__internal_6d2b816101cee40614dbfaaaefd2d14737016ddc51558014ba086d98b2c43605 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d2b816101cee40614dbfaaaefd2d14737016ddc51558014ba086d98b2c43605->enter($__internal_6d2b816101cee40614dbfaaaefd2d14737016ddc51558014ba086d98b2c43605_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a78e771d9f9b5d6910c184bc54ab0167939f8533223ee029d146e8e8fc424e9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a78e771d9f9b5d6910c184bc54ab0167939f8533223ee029d146e8e8fc424e9b->enter($__internal_a78e771d9f9b5d6910c184bc54ab0167939f8533223ee029d146e8e8fc424e9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 49
        echo "        ";
        $this->loadTemplate("navbar.html.twig", "base.html.twig", 49)->display($context);
        // line 50
        echo "
        <!-- /section:basics/navbar.layout -->
        <div class=\"main-container\" id=\"main-container\">
            <script type=\"text/javascript\">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {}
            </script>

            ";
        // line 59
        $this->loadTemplate("sidebar.html.twig", "base.html.twig", 59)->display($context);
        // line 60
        echo "
            <!-- /section:basics/sidebar -->
            <div class=\"main-content\">
                <!-- #section:basics/content.breadcrumbs -->
                <div class=\"breadcrumbs\" id=\"breadcrumbs\">
                    ";
        // line 78
        echo "                    <!-- /.breadcrumb -->

                    <!-- #section:basics/content.searchbox -->
                    <div class=\"nav-search\" id=\"nav-search\">
                        <form class=\"form-search\" action=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("search_snippet");
        echo "\">
                        <span class=\"input-icon\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"term\" placeholder=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("navbar.search", array(), "navbar"), "html", null, true);
        echo "\" class=\"nav-search-input\" id=\"nav-search-input\" autocomplete=\"off\" />
\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-search nav-search-icon\"></i>
\t\t\t\t\t\t\t</span>
                        </form>
                    </div>
                    <!-- /.nav-search -->

                    <!-- /section:basics/content.searchbox -->
                </div>

                <div class=\"page-header\">
                    <h1>
                        ";
        // line 96
        $this->displayBlock('contentHeadline', $context, $blocks);
        // line 97
        echo "                        ";
        // line 99
        echo "                    </h1>
                </div>

                <div class=\"page-content\">
                    <div class=\"row\">
                        <div class=\"col-xs-12\">
                            ";
        // line 105
        $this->displayBlock('content', $context, $blocks);
        // line 106
        echo "                        </div>
                    </div>
                </div>
            </div>

        </div>

    ";
        
        $__internal_a78e771d9f9b5d6910c184bc54ab0167939f8533223ee029d146e8e8fc424e9b->leave($__internal_a78e771d9f9b5d6910c184bc54ab0167939f8533223ee029d146e8e8fc424e9b_prof);

        
        $__internal_6d2b816101cee40614dbfaaaefd2d14737016ddc51558014ba086d98b2c43605->leave($__internal_6d2b816101cee40614dbfaaaefd2d14737016ddc51558014ba086d98b2c43605_prof);

    }

    // line 96
    public function block_contentHeadline($context, array $blocks = array())
    {
        $__internal_3a8273a366c0747ce742e8878605c13cc6110664cd8c68c715940cba3bf7a078 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a8273a366c0747ce742e8878605c13cc6110664cd8c68c715940cba3bf7a078->enter($__internal_3a8273a366c0747ce742e8878605c13cc6110664cd8c68c715940cba3bf7a078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contentHeadline"));

        $__internal_e13b64260daca0ef26351a1e4882ccba8820c61188851f8d48b48282d5d53daf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e13b64260daca0ef26351a1e4882ccba8820c61188851f8d48b48282d5d53daf->enter($__internal_e13b64260daca0ef26351a1e4882ccba8820c61188851f8d48b48282d5d53daf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contentHeadline"));

        
        $__internal_e13b64260daca0ef26351a1e4882ccba8820c61188851f8d48b48282d5d53daf->leave($__internal_e13b64260daca0ef26351a1e4882ccba8820c61188851f8d48b48282d5d53daf_prof);

        
        $__internal_3a8273a366c0747ce742e8878605c13cc6110664cd8c68c715940cba3bf7a078->leave($__internal_3a8273a366c0747ce742e8878605c13cc6110664cd8c68c715940cba3bf7a078_prof);

    }

    // line 105
    public function block_content($context, array $blocks = array())
    {
        $__internal_605c21f4c13e5f0d450efb761e96c14a20616f0688bb4a91d6a78952fb65627f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_605c21f4c13e5f0d450efb761e96c14a20616f0688bb4a91d6a78952fb65627f->enter($__internal_605c21f4c13e5f0d450efb761e96c14a20616f0688bb4a91d6a78952fb65627f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_cbadc2f98409dbbec4d323514d4f62ccffa522c25a0517875629c51e6a3e5e0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cbadc2f98409dbbec4d323514d4f62ccffa522c25a0517875629c51e6a3e5e0d->enter($__internal_cbadc2f98409dbbec4d323514d4f62ccffa522c25a0517875629c51e6a3e5e0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_cbadc2f98409dbbec4d323514d4f62ccffa522c25a0517875629c51e6a3e5e0d->leave($__internal_cbadc2f98409dbbec4d323514d4f62ccffa522c25a0517875629c51e6a3e5e0d_prof);

        
        $__internal_605c21f4c13e5f0d450efb761e96c14a20616f0688bb4a91d6a78952fb65627f->leave($__internal_605c21f4c13e5f0d450efb761e96c14a20616f0688bb4a91d6a78952fb65627f_prof);

    }

    // line 142
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_022dfdddfc37fa799c48216f9ac7da3ebbab89c72eda869bf51a12ebe2db9c01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_022dfdddfc37fa799c48216f9ac7da3ebbab89c72eda869bf51a12ebe2db9c01->enter($__internal_022dfdddfc37fa799c48216f9ac7da3ebbab89c72eda869bf51a12ebe2db9c01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_aa9919f6e4999d9c5acce54f0f3fc20f69210336bceb7c48711b955213c09f75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa9919f6e4999d9c5acce54f0f3fc20f69210336bceb7c48711b955213c09f75->enter($__internal_aa9919f6e4999d9c5acce54f0f3fc20f69210336bceb7c48711b955213c09f75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        
        $__internal_aa9919f6e4999d9c5acce54f0f3fc20f69210336bceb7c48711b955213c09f75->leave($__internal_aa9919f6e4999d9c5acce54f0f3fc20f69210336bceb7c48711b955213c09f75_prof);

        
        $__internal_022dfdddfc37fa799c48216f9ac7da3ebbab89c72eda869bf51a12ebe2db9c01->leave($__internal_022dfdddfc37fa799c48216f9ac7da3ebbab89c72eda869bf51a12ebe2db9c01_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 142,  402 => 105,  385 => 96,  368 => 106,  366 => 105,  358 => 99,  356 => 97,  354 => 96,  339 => 84,  334 => 82,  328 => 78,  321 => 60,  319 => 59,  308 => 50,  305 => 49,  296 => 48,  285 => 143,  283 => 142,  279 => 141,  275 => 140,  271 => 139,  266 => 137,  261 => 135,  257 => 134,  253 => 133,  249 => 132,  245 => 131,  241 => 130,  235 => 127,  231 => 126,  226 => 124,  222 => 123,  217 => 121,  208 => 115,  205 => 114,  203 => 48,  199 => 46,  196 => 45,  190 => 44,  181 => 41,  175 => 39,  170 => 38,  165 => 37,  163 => 36,  158 => 33,  149 => 32,  132 => 28,  115 => 9,  105 => 145,  103 => 32,  98 => 29,  96 => 28,  91 => 26,  87 => 25,  82 => 23,  77 => 21,  72 => 19,  68 => 18,  63 => 16,  59 => 15,  53 => 12,  49 => 11,  44 => 9,  39 => 7,  32 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'AdminBundle' %}

<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
    <title>{% block title %}{% endblock %}</title>

    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/bootstrap.min.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/font-awesome.min.css') }}\" />
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/ace-fonts.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/ace.min.css') }}\" />

    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/ace-skins.min.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/ace-rtl.min.css') }}\" />

    <script src=\"{{ asset('bundles/admin/js/ace-extra.min.js') }}\"></script>

    <link rel=\"stylesheet\" href=\"{{ asset('built/css/main.css') }}\">

    <link rel=\"stylesheet\" href=\"{{ asset('css/prism.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('built/css/main.css') }}\">

    {% block stylesheets %}{% endblock %}

</head>

{% block preBody %}
<body class=\"no-skin\">


    {% if app.request.hasPreviousSession %}
        {% for type, messages in app.session.flashbag.all() %}
            {% for message in messages %}
                <div class=\"alert alert-success alert-dismissable flash-{{ type }}\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    {{ message }}
                </div>
            {% endfor %}
        {% endfor %}
    {% endif %}


    {% block body %}
        {% include('navbar.html.twig') %}

        <!-- /section:basics/navbar.layout -->
        <div class=\"main-container\" id=\"main-container\">
            <script type=\"text/javascript\">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {}
            </script>

            {% include('sidebar.html.twig') %}

            <!-- /section:basics/sidebar -->
            <div class=\"main-content\">
                <!-- #section:basics/content.breadcrumbs -->
                <div class=\"breadcrumbs\" id=\"breadcrumbs\">
                    {#<script type=\"text/javascript\">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {}
                    </script>

                    <ul class=\"breadcrumb\">
                        <li>
                            #}{#<i class=\"ace-icon fa fa-home home-icon\"></i>#}{#
                            <a href=\"#\">{% block parent %}{% endblock %}</a>
                        </li>
                        <li class=\"active\">{% block child %}{% endblock %}</li>
                    </ul>#}
                    <!-- /.breadcrumb -->

                    <!-- #section:basics/content.searchbox -->
                    <div class=\"nav-search\" id=\"nav-search\">
                        <form class=\"form-search\" action=\"{{ url('search_snippet') }}\">
                        <span class=\"input-icon\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"term\" placeholder=\"{{ 'navbar.search'|trans({},'navbar') }}\" class=\"nav-search-input\" id=\"nav-search-input\" autocomplete=\"off\" />
\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-search nav-search-icon\"></i>
\t\t\t\t\t\t\t</span>
                        </form>
                    </div>
                    <!-- /.nav-search -->

                    <!-- /section:basics/content.searchbox -->
                </div>

                <div class=\"page-header\">
                    <h1>
                        {% block contentHeadline %}{% endblock %}
                        {#<br>
                        <small><i class=\"ace-icon fa fa-info\"></i> {% block contentSubHeadline %}Hier folgt ein Infotext{% endblock %}</small>#}
                    </h1>
                </div>

                <div class=\"page-content\">
                    <div class=\"row\">
                        <div class=\"col-xs-12\">
                            {% block content %}{% endblock %}
                        </div>
                    </div>
                </div>
            </div>

        </div>

    {% endblock %}

    <script src=\"{{ asset('bundles/admin/js/jquery.min.js') }}\"></script>

    <script type=\"text/javascript\">
        if('ontouchstart' in document.documentElement) document.write(\"<script src='./bundles/admin/js/jquery.mobile.custom.min.js'>\"+\"<\"+\"/script>\");
    </script>

    <script src=\"{{ asset('bundles/admin/js/bootstrap.min.js') }}\"></script>

    <script src=\"{{ asset('bundles/admin/js/ace-elements.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/ace.min.js') }}\"></script>

    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/css/ace.onpage-help.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/admin/js/themes/sunburst.css') }}\" />

    <script type=\"text/javascript\"> ace.vars['base'] = '..'; </script>
    <script src=\"{{ asset('bundles/admin/js/ace/ace.onpage-help.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/rainbow.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/language/generic.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/language/html.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/language/css.js') }}\"></script>
    <script src=\"{{ asset('bundles/admin/js/language/javascript.js') }}\"></script>

    <script src=\"{{ asset('bundles/admin/js/ace-extra.min.js') }}\"></script>

    <script src=\"{{ asset('js/clipboard.min.js') }}\"></script>
    <script src=\"{{ asset('js/prism.js') }}\"></script>
    <script src=\"{{ asset('built/js/app.js') }}\"></script>
    {% block javascript %}{% endblock %}
</body>
{% endblock %}
</html>", "base.html.twig", "/var/www/html/codesnippets/app/Resources/views/base.html.twig");
    }
}
