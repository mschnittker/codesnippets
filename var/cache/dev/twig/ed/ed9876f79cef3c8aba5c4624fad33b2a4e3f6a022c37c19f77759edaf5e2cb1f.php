<?php

/* sidebar.html.twig */
class __TwigTemplate_2ab1ab3ff9f1a051102954122b924df26d8e3fa0dc7cb71f1354922e43501eb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bfb4d675b3ed710bd9ce90fea08c943e4f55e9767b4e260666270f70e22acad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bfb4d675b3ed710bd9ce90fea08c943e4f55e9767b4e260666270f70e22acad->enter($__internal_0bfb4d675b3ed710bd9ce90fea08c943e4f55e9767b4e260666270f70e22acad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar.html.twig"));

        $__internal_7af968dd25d94fbde27ff2c88afa51d10163bb97d228b1427e5328c907a9e542 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7af968dd25d94fbde27ff2c88afa51d10163bb97d228b1427e5328c907a9e542->enter($__internal_7af968dd25d94fbde27ff2c88afa51d10163bb97d228b1427e5328c907a9e542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar.html.twig"));

        // line 2
        echo "
<!-- #section:basics/sidebar -->
<div id=\"sidebar\" class=\"sidebar                  responsive\">
    <script type=\"text/javascript\">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class=\"sidebar-shortcuts\" id=\"sidebar-shortcuts\">
        <div class=\"sidebar-shortcuts-large\" id=\"sidebar-shortcuts-large\">
            <button class=\"btn btn-success\">
                <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("my_snippets");
        echo "\" class=\"\" style=\"color: white;\">
                    <i class=\"ace-icon fa fa-folder-open-o\"></i>
                </a>
            </button>

            <button class=\"btn btn-info\">
                <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("new_snippet");
        echo "\" class=\"\">
                    <i class=\"ace-icon fa fa-pencil\" style=\"color: white;\"></i>
                </a>
            </button>

            <!-- #section:basics/sidebar.layout.shortcuts -->
            <button class=\"btn btn-warning\">
                <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("profile_show");
        echo "\" class=\"\">
                    <i class=\"ace-icon fa fa-users\" style=\"color: white;\"></i>
                </a>
            </button>

            <button class=\"btn btn-danger\">
                <a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("profile_edit");
        echo "\" class=\"\">
                    <i class=\"ace-icon fa fa-cogs\" style=\"color: white;\"></i>
                </a>
            </button>

            <!-- /section:basics/sidebar.layout.shortcuts -->
        </div>

        <div class=\"sidebar-shortcuts-mini\" id=\"sidebar-shortcuts-mini\">
           <span class=\"btn btn-success\"></span>

            <span class=\"btn btn-info\"></span>

            <span class=\"btn btn-warning\"></span>

            <span class=\"btn btn-danger\"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class=\"nav nav-list\">
        <li class=\"\">
            <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_backend");
        echo "\">
                <i class=\"menu-icon fa fa-tachometer\"></i>
                <span class=\"menu-text\"> ";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("dashboard.parent", array(), "sidebar"), "html", null, true);
        echo " </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

        ";
        // line 60
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 61
            echo "<li class=\"treeview\">
                <a href=\"#\" class=\"dropdown-toggle\">
                    <i class=\"menu-icon fa fa-cogs\"></i>
                    <span class=\"menu-text\"> ";
            // line 64
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("settings.parent", array(), "sidebar"), "html", null, true);
            echo " </span>

                    <b class=\"arrow fa fa-angle-down\"></b>
                </a>

                <b class=\"arrow\"></b>

                <ul class=\"submenu\">
                    <li class=\"\">
                        <a href=\"";
            // line 73
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("list_users");
            echo "\" class=\"\">
                            <i class=\"menu-icon fa fa-caret-right\"></i>

                            ";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("settings.users", array(), "sidebar"), "html", null, true);
            echo "
                            <b class=\"arrow\"></b>
                        </a>

                        <b class=\"arrow\"></b>
                    </li>
                    ";
            // line 92
            echo "                </ul>
            </li>
        ";
        }
        // line 95
        echo "
        <li class=\"\">
            <a href=\"";
        // line 97
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("my_snippets");
        echo "\">
                <i class=\"menu-icon fa fa-folder-open-o\"></i>
                <span class=\"menu-text\"> ";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("snippets.my_snippets", array(), "sidebar"), "html", null, true);
        echo " </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

        <li class=\"\">
            <a href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("new_snippet");
        echo "\">
                <i class=\"menu-icon fa fa-pencil\"></i>
                <span class=\"menu-text\"> ";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("snippets.new_snippets", array(), "sidebar"), "html", null, true);
        echo " </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

    </ul><!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class=\"sidebar-toggle sidebar-collapse\" id=\"sidebar-collapse\">
        <i class=\"ace-icon fa fa-angle-double-left\" data-icon1=\"ace-icon fa fa-angle-double-left\" data-icon2=\"ace-icon fa fa-angle-double-right\"></i>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
    <script src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/admin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}


        var url = window.location;

        \$('ul.nav-list a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        \$('ul.submenu a').filter(function() {
            return this.href == url;
        }).parentsUntil(\".nav-list > .submenu\").addClass('active');
    </script>
</div>";
        
        $__internal_0bfb4d675b3ed710bd9ce90fea08c943e4f55e9767b4e260666270f70e22acad->leave($__internal_0bfb4d675b3ed710bd9ce90fea08c943e4f55e9767b4e260666270f70e22acad_prof);

        
        $__internal_7af968dd25d94fbde27ff2c88afa51d10163bb97d228b1427e5328c907a9e542->leave($__internal_7af968dd25d94fbde27ff2c88afa51d10163bb97d228b1427e5328c907a9e542_prof);

    }

    public function getTemplateName()
    {
        return "sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 122,  166 => 108,  161 => 106,  151 => 99,  146 => 97,  142 => 95,  137 => 92,  128 => 76,  122 => 73,  110 => 64,  105 => 61,  103 => 60,  94 => 54,  89 => 52,  65 => 31,  56 => 25,  46 => 18,  37 => 12,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'AdminBundle' %}

<!-- #section:basics/sidebar -->
<div id=\"sidebar\" class=\"sidebar                  responsive\">
    <script type=\"text/javascript\">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class=\"sidebar-shortcuts\" id=\"sidebar-shortcuts\">
        <div class=\"sidebar-shortcuts-large\" id=\"sidebar-shortcuts-large\">
            <button class=\"btn btn-success\">
                <a href=\"{{ url('my_snippets') }}\" class=\"\" style=\"color: white;\">
                    <i class=\"ace-icon fa fa-folder-open-o\"></i>
                </a>
            </button>

            <button class=\"btn btn-info\">
                <a href=\"{{ url('new_snippet') }}\" class=\"\">
                    <i class=\"ace-icon fa fa-pencil\" style=\"color: white;\"></i>
                </a>
            </button>

            <!-- #section:basics/sidebar.layout.shortcuts -->
            <button class=\"btn btn-warning\">
                <a href=\"{{ url('profile_show') }}\" class=\"\">
                    <i class=\"ace-icon fa fa-users\" style=\"color: white;\"></i>
                </a>
            </button>

            <button class=\"btn btn-danger\">
                <a href=\"{{ url('profile_edit') }}\" class=\"\">
                    <i class=\"ace-icon fa fa-cogs\" style=\"color: white;\"></i>
                </a>
            </button>

            <!-- /section:basics/sidebar.layout.shortcuts -->
        </div>

        <div class=\"sidebar-shortcuts-mini\" id=\"sidebar-shortcuts-mini\">
           <span class=\"btn btn-success\"></span>

            <span class=\"btn btn-info\"></span>

            <span class=\"btn btn-warning\"></span>

            <span class=\"btn btn-danger\"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class=\"nav nav-list\">
        <li class=\"\">
            <a href=\"{{ url('admin_backend') }}\">
                <i class=\"menu-icon fa fa-tachometer\"></i>
                <span class=\"menu-text\"> {{ 'dashboard.parent'|trans({},'sidebar') }} </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

        {% if is_granted('ROLE_SUPER_ADMIN') -%}
            <li class=\"treeview\">
                <a href=\"#\" class=\"dropdown-toggle\">
                    <i class=\"menu-icon fa fa-cogs\"></i>
                    <span class=\"menu-text\"> {{ 'settings.parent'|trans({},'sidebar') }} </span>

                    <b class=\"arrow fa fa-angle-down\"></b>
                </a>

                <b class=\"arrow\"></b>

                <ul class=\"submenu\">
                    <li class=\"\">
                        <a href=\"{{ url('list_users') }}\" class=\"\">
                            <i class=\"menu-icon fa fa-caret-right\"></i>

                            {{ 'settings.users'|trans({},'sidebar') }}
                            <b class=\"arrow\"></b>
                        </a>

                        <b class=\"arrow\"></b>
                    </li>
                    {#<li class=\"\">
                        <a href=\"{{ url('profile_show') }}\" class=\"\">
                            <i class=\"menu-icon fa fa-caret-right\"></i>

                            {{ 'settings.logs'|trans({},'sidebar') }}
                            <b class=\"arrow\"></b>
                        </a>

                        <b class=\"arrow\"></b>
                    </li>#}
                </ul>
            </li>
        {% endif %}

        <li class=\"\">
            <a href=\"{{ url('my_snippets') }}\">
                <i class=\"menu-icon fa fa-folder-open-o\"></i>
                <span class=\"menu-text\"> {{ 'snippets.my_snippets'|trans({},'sidebar') }} </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

        <li class=\"\">
            <a href=\"{{ url('new_snippet') }}\">
                <i class=\"menu-icon fa fa-pencil\"></i>
                <span class=\"menu-text\"> {{ 'snippets.new_snippets'|trans({},'sidebar') }} </span>
            </a>

            <b class=\"arrow\"></b>
        </li>

    </ul><!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class=\"sidebar-toggle sidebar-collapse\" id=\"sidebar-collapse\">
        <i class=\"ace-icon fa fa-angle-double-left\" data-icon1=\"ace-icon fa fa-angle-double-left\" data-icon2=\"ace-icon fa fa-angle-double-right\"></i>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
    <script src=\"{{ asset('bundles/admin/js/jquery.min.js') }}\"></script>
    <script type=\"text/javascript\">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}


        var url = window.location;

        \$('ul.nav-list a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        \$('ul.submenu a').filter(function() {
            return this.href == url;
        }).parentsUntil(\".nav-list > .submenu\").addClass('active');
    </script>
</div>", "sidebar.html.twig", "/var/www/html/codesnippets/app/Resources/views/sidebar.html.twig");
    }
}
