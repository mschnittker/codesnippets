<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_788e43b6650382b138d16fd1a87f245052c93f084cc1fa2f20f2aadd148b2287 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec131ca733ba5da34df092adc0782d118a53e16eddecfb5debcaffa6a46e246c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec131ca733ba5da34df092adc0782d118a53e16eddecfb5debcaffa6a46e246c->enter($__internal_ec131ca733ba5da34df092adc0782d118a53e16eddecfb5debcaffa6a46e246c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_42476533e2a0c4e3ea9da462dfbc95d9ff386c861781158787357ce03a5b4f8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42476533e2a0c4e3ea9da462dfbc95d9ff386c861781158787357ce03a5b4f8a->enter($__internal_42476533e2a0c4e3ea9da462dfbc95d9ff386c861781158787357ce03a5b4f8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec131ca733ba5da34df092adc0782d118a53e16eddecfb5debcaffa6a46e246c->leave($__internal_ec131ca733ba5da34df092adc0782d118a53e16eddecfb5debcaffa6a46e246c_prof);

        
        $__internal_42476533e2a0c4e3ea9da462dfbc95d9ff386c861781158787357ce03a5b4f8a->leave($__internal_42476533e2a0c4e3ea9da462dfbc95d9ff386c861781158787357ce03a5b4f8a_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7eb8904713022ae64db4d552fc68a917deff88125f7902c7bc3e14f6bd931a71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7eb8904713022ae64db4d552fc68a917deff88125f7902c7bc3e14f6bd931a71->enter($__internal_7eb8904713022ae64db4d552fc68a917deff88125f7902c7bc3e14f6bd931a71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_29fd89d0652e6b47b3c20d7d132e75b2a12adb2ce7c2a30d2ad9dc2bb22b6bfd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29fd89d0652e6b47b3c20d7d132e75b2a12adb2ce7c2a30d2ad9dc2bb22b6bfd->enter($__internal_29fd89d0652e6b47b3c20d7d132e75b2a12adb2ce7c2a30d2ad9dc2bb22b6bfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_29fd89d0652e6b47b3c20d7d132e75b2a12adb2ce7c2a30d2ad9dc2bb22b6bfd->leave($__internal_29fd89d0652e6b47b3c20d7d132e75b2a12adb2ce7c2a30d2ad9dc2bb22b6bfd_prof);

        
        $__internal_7eb8904713022ae64db4d552fc68a917deff88125f7902c7bc3e14f6bd931a71->leave($__internal_7eb8904713022ae64db4d552fc68a917deff88125f7902c7bc3e14f6bd931a71_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/var/www/html/codesnippets/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
