<?php
namespace AppBundle\Services;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;


class PaginationService
{
    /**
     * @param Query $query
     * @param int $pageSize
     * @return float
     */
    public static function getPagesCount(Query $query, $pageSize = 10)
    {
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return ceil($paginator->count() / $pageSize);
    }

    /**
     * @param Query $query
     * @param int $pageSize
     * @param int $currentPage
     * @return array
     */
    public static function paginate(Query $query, $pageSize = 10, $currentPage = 1)
    {
        $pageSize = (int)$pageSize;
        $currentPage = (int)$currentPage;

        if ($pageSize < 1) {
            $pageSize = 10;
        }

        if ($currentPage < 1) {
            $currentPage = 1;
        }

        $paginator = new Paginator($query);

        $results = $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($currentPage - 1))
            ->setMaxResults($pageSize)
            ->getResult();

        return $results;
    }
}