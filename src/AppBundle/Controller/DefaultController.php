<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Code;
use AppBundle\Entity\Comments;
use AppBundle\Form\commentType;
use AppBundle\Form\snippetType;
use AppBundle\Services\PaginationService;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mySnippetsAction($page = 1)
    {
        $results = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Code')
            ->getResultsForPagination($this->getUser());

        $pages = PaginationService::getPagesCount($results);
        $content = PaginationService::paginate($results, 10, $page);

        return $this->render('AppBundle:default:my-snippets.html.twig', array(
            'results' => $content,
            'page' => $page,
            'pages' => $pages
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newSnippetAction(Request $request, $page = 1)
    {
        $entity = new Code();

        $form = $this->createForm(snippetType::class, $entity, array('method' => 'POST'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $results = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Code')
                ->getResultsForPagination($this->getUser());

            $pages = PaginationService::getPagesCount($results);
            $content = PaginationService::paginate($results, 10, $page);

            return $this->render('AppBundle:default:my-snippets.html.twig', array(
                'results' => $content,
                'page' => $page,
                'pages' => $pages
            ));
        }


        return $this->render('AppBundle:default:new-snippet.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function snippetDetailsAction($id = 1, Request $request, $page = 1)
    {
        $entity = new Comments();

        $form = $this->createForm(commentType::class, $entity, array('method' => 'POST'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }

        $own = $this
                    ->getDoctrine()
                    ->getRepository('AppBundle:Code')
                    ->getOneResultByUser($id, $this->getUser());

        $public = $this
                    ->getDoctrine()
                    ->getRepository('AppBundle:Code')
                    ->getOneResultByPublic($id);

        $comments = $this
                    ->getDoctrine()
                    ->getRepository('AppBundle:Comments')
                    ->getCommentsById($id);

        if(!$own && !$public){
            return $this->render('TwigBundle:Exception:error404.html.twig',array());
        }

        return $this->render('AppBundle:default:snippet-details.html.twig', array(
            'result' => $public ?: $own,
            'comments' => $comments,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param int $id
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteSnippetAction($id = 1, $page = 1)
    {
        $em = $this
                    ->getDoctrine()
                    ->getEntityManager();

        $item = $em
                    ->getRepository('AppBundle:Code')
                    ->getOneResultByUser($id, $this->getUser());

        $em->remove($item);
        $em->flush();

        $results = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Code')
                    ->getResultsForPagination($this->getUser());

        $pages = PaginationService::getPagesCount($results);
        $content = PaginationService::paginate($results, 10, $page);

        return $this->render('AppBundle:default:my-snippets.html.twig', array(
            'results' => $content,
            'page' => $page,
            'pages' => $pages
        ));
    }

    /**
     * @param Request $request
     * @param int $id
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function editSnippetAction(Request $request, $id = 1, $page = 1)
    {
        $edit = $this->getDoctrine()
            ->getRepository('AppBundle:Code')
            ->find($id);

        $form = $this->createForm(snippetType::class, $edit);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($edit);
            $em->flush();

            $results = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Code')
                    ->getResultsForPagination($this->getUser());

            $pages = PaginationService::getPagesCount($results);
            $content = PaginationService::paginate($results, 10, $page);

            return $this->render('AppBundle:default:my-snippets.html.twig', array(
                'results' => $content,
                'page' => $page,
                'pages' => $pages
            ));
        }

        return $this->render('AppBundle:default:edit-snippet.html.twig',array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchSnippetAction(Request $request)
    {
        $term  = $request->get('term');

        $own   = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Code')
                    ->getSearchResults($term, $this->getUser());

        $public = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Code')
                    ->getPublicResults($term, $this->getUser());

        return $this->render('AppBundle:default:search-page.html.twig', array(
            'results' => array_merge($own, $public)
        ));
    }
}
