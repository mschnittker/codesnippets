<?php

namespace AppBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class commentType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * TestType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = $this->container->get('translator');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $builder
            ->add('user', HiddenType::class, array(
                'label' => false,
                'data' => $user,
                'attr' => array(
                    'placeholder' => '',
                    'class' => 'form-control'
                )
            ))
            ->add('code_id', HiddenType::class, array(
                'label' => false,
                'data' => '',
                'attr' => array(
                    'placeholder' => '',
                    'class' => 'form-control'
                )
            ))
            ->add('message', CKEditorType::class, array(
                'label' => $translator->trans('form.comments',[], 'forms'),
                'config' => array(
                    'toolbar' => 'big_toolbar'
                ),
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comments'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_comment_type';
    }


}
