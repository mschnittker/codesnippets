<?php

namespace AppBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class snippetType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * TestType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = $this->container->get('translator');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $builder
            ->add('user', HiddenType::class, array(
                'label' => false,
                'data' => $user,
                'attr' => array(
                    'placeholder' => '',
                    'class' => 'form-control'
                )
            ))
            ->add('public', ChoiceType::class, array(
                'label' => $translator->trans('form.public',[], 'forms'),
                'choices' => array(
                    $translator->trans('public.true',[], 'frontend') => true,
                    $translator->trans('public.false',[], 'frontend') => false
                ),
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('language', ChoiceType::class, array(
                'label' => $translator->trans('form.language',[], 'forms'),
                'choices' => array(
                    "Markup" => "markup",
                    "CSS" => "css",
                    "C-like" => "clike",
                    "JavaScript" => "javascript",
                    "ABAP" => "abap",
                    "ActionScript" => "actionscript",
                    "Ada" => "ada",
                    "Apache Configuration" => "apacheconf",
                    "APL" => "apl",
                    "AppleScript" => "applescript",
                    "Arduino" => "arduino",
                    "AsciiDoc" => "asciidoc",
                    "ASP.NET (C#)" => "aspnet",
                    "AutoIt" => "autoit",
                    "AutoHotkey" => "autohotkey",
                    "Bash" => "bash",
                    "BASIC" => "basic",
                    "Batch" => "batch",
                    "Bison" => "bison",
                    "Brainfuck" => "brainfuck",
                    "Bro" => "bro",
                    "C" => "c",
                    "C#" => "csharp",
                    "C++" => "cpp",
                    "CoffeeScript" => "coffeescript",
                    "Crystal" => "crystal",
                    "CSS Extras" => "css-extras",
                    "D" => "d",
                    "Dart" => "dart",
                    "Django/Jinja2" => "django",
                    "Diff" => "diff",
                    "Docker" => "docker",
                    "Eiffel" => "eiffel",
                    "Elixir" => "elixir",
                    "Erlang" => "erlang",
                    "F#" => "fsharp",
                    "Fortran" => "fortran",
                    "Gherkin" => "gherkin",
                    "Git" => "git",
                    "GLSL" => "glsl",
                    "Go" => "go",
                    "GraphQL" => "graphql",
                    "Groovy" => "groovy",
                    "Haml" => "haml",
                    "Handlebars" => "handlebars",
                    "Haskell" => "haskell",
                    "Haxe" => "haxe",
                    "HTTP" => "http",
                    "Icon" => "icon",
                    "Inform 7" => "inform7",
                    "Ini" => "ini",
                    "J" => "j",
                    "Java" => "java",
                    "Jolie" => "jolie",
                    "JSON" => "json",
                    "Julia" => "julia",
                    "Keyman" => "keyman",
                    "Kotlin" => "kotlin",
                    "LaTeX" => "latex",
                    "Less" => "less",
                    "LiveScript" => "livescript",
                    "LOLCODE" => "lolcode",
                    "Lua" => "lua",
                    "Makefile" => "makefile",
                    "Markdown" => "markdown",
                    "MATLAB" => "matlab",
                    "MEL" => "mel",
                    "Mizar" => "mizar",
                    "Monkey" => "monkey",
                    "N4JS" => "n4js",
                    "NASM" => "nasm",
                    "nginx" => "nginx",
                    "Nim" => "nim",
                    "Nix" => "nix",
                    "NSIS" => "nsis",
                    "Objective-C" => "objectivec",
                    "OCaml" => "ocaml",
                    "OpenCL" => "opencl",
                    "Oz" => "oz",
                    "PARI/GP" => "parigp",
                    "Parser" => "parser",
                    "Pascal" => "pascal",
                    "Perl" => "perl",
                    "PHP" => "php",
                    "PHP Extras" => "php-extras",
                    "PowerShell" => "powershell",
                    "Processing" => "processing",
                    "Prolog" => "prolog",
                    ".properties" => "properties",
                    "Protocol Buffers" => "protobuf",
                    "Pug" => "pug",
                    "Puppet" => "puppet",
                    "Pure" => "pure",
                    "Python" => "python",
                    "Q" => "q",
                    "Qore" => "qore",
                    "R" => "r",
                    "React JSX" => "jsx",
                    "Ren'py" => "renpy",
                    "Reason" => "reason",
                    "reST (reStructuredText)" => "rest",
                    "Rip" => "rip",
                    "Roboconf" => "roboconf",
                    "Ruby" => "ruby",
                    "Rust" => "rust",
                    "SAS" => "sas",
                    "Sass (Sass)" => "sass",
                    "Sass (Scss)" => "scss",
                    "Scala" => "scala",
                    "Scheme" => "scheme",
                    "Smalltalk" => "smalltalk",
                    "Smarty" => "smarty",
                    "SQL" => "sql",
                    "Stylus" => "stylus",
                    "Swift" => "swift",
                    "Tcl" => "tcl",
                    "Textile" => "textile",
                    "Twig" => "twig",
                    "TypeScript" => "typescript",
                    "VB.Net" => "vbnet",
                    "Verilog" => "verilog",
                    "VHDL" => "vhdl",
                    "vim" => "vim",
                    "Wiki markup" => "wiki",
                    "Xojo (REALbasic)" => "xojo",
                    "YAML" => "yaml",
                ),
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('description', TextType::class, array(
                'label' => $translator->trans('form.description',[], 'forms'),
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('snippet', CKEditorType::class, array(
                'label' => $translator->trans('form.snippet',[], 'forms'),
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('information', CKEditorType::class, array(
                'label' => $translator->trans('form.information',[], 'forms'),
                'config' => array(
                    'toolbar' => 'big_toolbar'
                ),
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Code'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_snippet_type';
    }


}
