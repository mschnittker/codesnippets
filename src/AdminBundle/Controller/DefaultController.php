<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @return Response
     *
     * Dashboard
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig', array());
    }

    /**
     * @return Response
     */
    public function usersAction()
    {
        $users = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:User')
                    ->getAllUsers();

        return $this->render('AdminBundle:Default:users.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @param int $id
     * @return Response
     */
    public function userDetailsAction($id = 1)
    {
        $user = $this
            ->getDoctrine()
            ->getRepository('AdminBundle:User')
            ->getOneResultByUserId($id);


        return $this->render('AdminBundle:Default:user-details.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * @param int $id
     * @return Response
     */
    public function deleteUserAction($id = 1)
    {
        $em = $this
            ->getDoctrine()
            ->getEntityManager();

        $item = $em
            ->getRepository('AdminBundle:User')
            ->getOneResultByUserId($id);

        $em->remove($item);
        $em->flush();

        $users = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AdminBundle:User')
            ->getAllUsers();

        return $this->render('AdminBundle:Default:users.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @return Response
     */
    public function editUserAction()
    {
        $users = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AdminBundle:User')
            ->getAllUsers();

        return $this->render('AdminBundle:Default:users.html.twig', array(
            'users' => $users
        ));
    }
}
