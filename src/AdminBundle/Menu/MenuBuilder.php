<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 10.10.17
 * Time: 09:21
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $translator = $this->container->get('translator');
        $menu = $factory->createItem('root');

        $menu->addChild($translator->trans('navbar.start', array(), 'navbar'), array('route' => 'homepage'));
        $menu->addChild($translator->trans('navbar.privacy_policy', array(), 'navbar'), array('route' => 'privacy_policy'));
        $menu->addChild($translator->trans('navbar.imprint', array(), 'navbar'), array('route' => 'imprint'));


        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function loginMenu(FactoryInterface $factory, array $options)
    {
        $translator = $this->container->get('translator');
        $menu = $factory->createItem('root');

        $menu->addChild($translator->trans('navbar.admin.register', array(), 'navbar'), array('route' => 'fos_user_registration_register'));
        $menu->addChild($translator->trans('navbar.admin.login', array(), 'navbar'), array('route' => 'admin_login'));

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function logoutMenu(FactoryInterface $factory, array $options)
    {
        $translator = $this->container->get('translator');
        $menu = $factory->createItem('root');

        $menu->addChild($translator->trans('navbar.admin.logout', array(), 'navbar'), array('route' => 'fos_user_security_logout'));

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function adminMenu(FactoryInterface $factory, array $options)
    {
        $translator = $this->container->get('translator');
        $menu = $factory->createItem('root');

        $menu->addChild($translator->trans('navbar.admin.page', array(), 'navbar'), array('route' => 'my_snippets'));
        $menu->addChild($translator->trans('navbar.admin.new', array(), 'navbar'), array('route' => 'new_snippet'));

        return $menu;
    }
}