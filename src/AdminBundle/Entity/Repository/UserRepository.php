<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 27.11.17
 * Time: 11:46
 */

namespace AdminBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getAllUsers()
    {
        return $this
            ->createQueryBuilder('user')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOneResultByUserId($id)
    {
        return $this
            ->createQueryBuilder('user')
            ->where('user.id LIKE :id')
            ->setParameter('id', '%'.$id.'%')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
