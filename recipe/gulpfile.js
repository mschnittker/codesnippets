var gulp = require('gulp');

var concat = require ('gulp-concat'),
    minifyJs = require('gulp-js-minify'),
    typeScript = require('gulp-typescript'),
    imagemin = require ('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    sass = require('gulp-sass'),
    minifyCss = require ('gulp-minify-css'),
    cleanCss = require('gulp-clean-css');

var imageInput = './images/*',
    imageOutput = '../web/built/images',
    sassInput = './sass/*',
    sassOutput = '../web/built/css',
    jsInput = './js/*',
    jsOutput = '../web/built/js',
    typesScriptInput = './js/*.ts',
    typeScriptOutput = './js';

gulp.task('images', function () {
    return gulp
        .src(imageInput)
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest(imageOutput));
});

gulp.task('sass', function () {
    return gulp
        .src(sassInput)
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(gulp.dest(sassOutput));

});

gulp.task('ts', function () {
    return gulp
        .src(typesScriptInput)
        .pipe(typeScript({
            noImplicitAny: true,
            outFile: 'temp.js'
        }))
        .pipe(gulp.dest(typeScriptOutput));
});

gulp.task('js', function() {
    return gulp
        .src(jsInput)
        .pipe(concat('app.js'))
        .pipe(minifyJs())
        .pipe(gulp.dest(jsOutput));
});

gulp.task('watch', function() {
    return gulp
        .watch(sassInput, ['sass'])
        .on('change', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        });
});

gulp.task('default', ['sass', 'js', 'images']);
