# CodeSnippets v2.0 #

CodeSnippets is a small code manager written in php using the Symfony Framework. 
Users can be created, and users can share their code with other users.

## install : ##

### Step 1 ###

```
create file parameters.yml
```

### Step 2 ###

```console
composer install
```

### Step 3 ###

```console
bin/console doctrine:schema:update --force
```

### Step 4 ###

```console
sudo chown -R www-data:www-data ./var/cache
```

### Step 5 ###

```console
sudo rm -Rf ./var/cache/*
```

### Step 6 ###

```console
bin/console assets:install --symlink web
```

## External Bundles : ##

* KNPMenuBundle
* FOSUserBundle
* IvoryCKEditorBundle


