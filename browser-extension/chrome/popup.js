$(function(){
    var url = 'http://127.0.0.1/codesnippets/web/app.php/admin';
    init.search(url);
    init.newSnippet(url);
    init.mySnippets(url);
});

var init = {
    'search': function (url) {
        $('#search').on('keypress', function (e) {
            if(e.which === 13){
                var href = url + "/search?term=" + $("input[name='term']").val();
                chrome.tabs.create({active: true, url: href});
                this.displayAsTab = true;
                return false;
            }
        });
    },
    'newSnippet': function (url) {
        $('#new-snippet').on('click', function(){
            var href = url + "/new-snippet";
            chrome.tabs.create({active: true, url: href});
            this.displayAsTab = true;
            return false;
        });
    },
    'mySnippets': function (url) {
        $('#my-snippets').on('click', function(){
            var href = url + "/my-snippets";
            chrome.tabs.create({active: true, url: href});
            this.displayAsTab = true;
            return false;
        });
    }
}
